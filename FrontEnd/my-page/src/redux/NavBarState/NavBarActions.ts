interface IShowBlog {
    type: "ShowBlog";
}

interface IShowResume { 
    type: "ShowResume"
}

interface IShowAboutMarco {
    type: "ShowAboutMarco";
}

interface IShowProjects {
    type: "ShowProjects";
}

interface IShowMusic {
    type: "ShowMusic";
}

export const showBlog = () : IShowBlog => ({
    type: "ShowBlog" 
})

export const showResume = () : IShowResume => ({
    type: "ShowResume"
})

export const showAboutMarco = () : IShowAboutMarco => ({
    type: "ShowAboutMarco"
})

export const showProjects = () : IShowProjects => ({
    type: "ShowProjects"
})

export const showMusic = () : IShowMusic => ({
    type: "ShowMusic"
})

export type NavBarAction = IShowBlog | IShowResume | IShowAboutMarco | IShowProjects | IShowMusic;
