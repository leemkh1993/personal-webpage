export interface NavBarState {
    isFirstLoad: Boolean,
    isBlog: Boolean,
    isResume: Boolean,
    isAboutMarco: Boolean,
    isProjects: Boolean,
    isMusic: Boolean
}

export const initNavBarState = {
    isFirstLoad: true,
    isBlog: false,
    isResume: false,
    isAboutMarco: false,
    isProjects: false,
    isMusic: false
}