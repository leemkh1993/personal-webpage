import { NavBarState, initNavBarState } from './NavBarState';
import { NavBarAction } from './NavBarActions';

export const navBarReducer = (state: NavBarState = initNavBarState, action: NavBarAction): NavBarState => {
    switch (action.type) {
        case ("ShowBlog"):
            state = initNavBarState
            return {
                ...state,
                isFirstLoad: false,
                isBlog: true
            }

        case ("ShowResume"):
            state = initNavBarState
            return {
                ...state,
                isFirstLoad: false,
                isResume: true
            }

        case ("ShowAboutMarco"):
            state = initNavBarState
            return {
                ...state,
                isFirstLoad: false,
                isAboutMarco: true
            }

        case ("ShowProjects"):
            state = initNavBarState
            return {
                ...state,
                isFirstLoad: false,
                isProjects: true
            }

        case ("ShowMusic"):
            state = initNavBarState
            return {
                ...state,
                isFirstLoad: false,
                isMusic: true
            }
        
        default:
            return state;
    }
}