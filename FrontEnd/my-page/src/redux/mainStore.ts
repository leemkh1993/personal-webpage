import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { NavBarAction } from './NavBarState/NavBarActions';
import logger from 'redux-logger';
import { NavBarState } from './NavBarState/NavBarState';
import { navBarReducer } from './NavBarState/NavBarReducer';


// The old way to do create states
// Step 1: Create state interface
// export interface StoreState {
//     isFirstLoad: Boolean,
//     isBlog: Boolean,
//     isResume: Boolean,
//     isAboutMarco: Boolean,
//     isProjects: Boolean,
//     isMusic: Boolean
// }

// Step 2: Set the initial state
// const initialState = {
//     isFirstLoad: true,
//     isBlog: false,
//     isResume: false,
//     isAboutMarco: false,
//     isProjects: false,
//     isMusic: false
// }

// Step 3: Reducer that handles action and change in state
// const rootReducer = (state: StoreState = initialState, action: NavBarAction): StoreState => {
//     switch (action.type) {
//         case ("ShowBlog"):
//             state = initialState
//             return {
//                 ...state,
//                 isFirstLoad: false,
//                 isBlog: true
//             }

//         case ("ShowResume"):
//             state = initialState
//             return {
//                 ...state,
//                 isFirstLoad: false,
//                 isResume: true
//             }

//         case ("ShowAboutMarco"):
//             state = initialState
//             return {
//                 ...state,
//                 isFirstLoad: false,
//                 isAboutMarco: true
//             }

//         case ("ShowProjects"):
//             state = initialState
//             return {
//                 ...state,
//                 isFirstLoad: false,
//                 isProjects: true
//             }

//         case ("ShowMusic"):
//             state = initialState
//             return {
//                 ...state,
//                 isFirstLoad: false,
//                 isMusic: true
//             }
        
//         default:
//             return state;
//     }
// }

// Step 4: createStore with generic type of state and action, reducer as argument
// export default createStore<StoreState, NavBarAction, {}, {}>(rootReducer);

export interface IntegratedStates{
    navBar: NavBarState
};
// Step 1 - Unite state interfaces

// Step 2 - Unite actions of different states
type IntegratedAction = NavBarAction;

// Step 3 - Combine reducers
const mainReducer = combineReducers({
    navBar: navBarReducer
})

// Redux middleware
declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

// When more than 1 middleware are used
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Step 4 - Create Store 
export default createStore<IntegratedStates, IntegratedAction, {}, {}>(
    mainReducer,
    composeEnhancers(
        applyMiddleware(logger)
    )
);