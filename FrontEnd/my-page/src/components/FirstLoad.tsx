import React from 'react';
import '../CSS/FirstLoad.css';

const FirstLoad: React.FC = () => {
    return (
        <div id="firstLoadText">
            <span id="quote">
                Manner Maketh Man
            </span>
            <span id="source">
                - Harry Hart (King's Man - The Secret Service)
            </span>
        </div>
    )
};

export default FirstLoad;