import React from 'react';
import '../CSS/Main.css';
import { IntegratedStates } from '../redux/mainStore';
import {useSelector} from 'react-redux';
import FirstLoad from './FirstLoad';
import Blog from './Blog';
import Resume from './Resume';
import AboutMarco from './AboutMarco';
import Projects from './Projects';
import Music from './Music';

const MainBody: React.FC = () => {
  const isFirstLoad = useSelector((state: IntegratedStates) => state.navBar.isFirstLoad);
  const isBlog = useSelector((state: IntegratedStates) => state.navBar.isBlog);
  const isResume = useSelector((state: IntegratedStates) => state.navBar.isResume);
  const isAboutMarco = useSelector((state: IntegratedStates) => state.navBar.isAboutMarco);
  const isProjects = useSelector((state: IntegratedStates) => state.navBar.isProjects);
  const isMusic = useSelector ((state: IntegratedStates) => state.navBar.isMusic)

  return (
    <div id='mainBody'>
      {isFirstLoad && <FirstLoad />}
      {isBlog && <Blog />}
      {isResume && <Resume />}
      {isAboutMarco && <AboutMarco />}
      {isProjects && <Projects />}
      {isMusic && <Music />}
    </div>
  );
}

export default MainBody;
