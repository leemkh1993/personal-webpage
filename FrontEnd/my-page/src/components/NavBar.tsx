import React from 'react';
import "../CSS/NavContainer.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'reactstrap';
import { useDispatch } from 'react-redux';
import { showBlog, showResume, showAboutMarco, showProjects, showMusic } from '../redux/NavBarState/NavBarActions';


const NavBar: React.FC = () => {
    const dispatch = useDispatch();

    const blogClickHandler = () => {
        const blog = showBlog();
        dispatch(blog);
    };

    const resumeClickHandler = () => {
        const resume = showResume();
        dispatch(resume);
    };

    const aboutMarcoClickHandler = () => {
        const aboutMarco = showAboutMarco();
        dispatch(aboutMarco);
    };

    const projectsClickHandler = () => {
        const projects = showProjects();
        dispatch(projects);
    };

    const musicClickHandler = () => {
        const music = showMusic();
        dispatch(music);
    };

    return (
        <Container>
            <Row className = "row">
                <Col xs="6" md="2" className="labelContainer">
                    <div className="secondLabel" onClick={blogClickHandler}>
                        Blog
                    </div>
                </Col>
                <Col xs="6" md="2" className="labelContainer">
                    <div className="secondLabel" onClick={resumeClickHandler}>
                        Resume
                    </div>
                </Col>
                <Col xs="12" md="4" className="labelContainer">
                    <div className="mainLabel" onClick={aboutMarcoClickHandler}>
                        About Marco
                    </div>
                </Col>
                <Col xs="6" md="2" className="labelContainer">
                    <div className="secondLabel" onClick={projectsClickHandler}>
                        Projects
                    </div>
                </Col>
                <Col xs="6" md="2" className="labelContainer">
                    <div className="secondLabel" onClick={musicClickHandler}>
                        Music
                    </div>
                </Col>
            </Row>
        </Container>
    );
}

export default NavBar;