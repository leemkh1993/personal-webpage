import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import MainBody from './components/Main';
import NavBar from './components/NavBar';
import { Provider } from 'react-redux';
import store from './redux/mainStore';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <MainBody />
    </Provider>
  </React.StrictMode>,
  document.getElementById('mainContainer')
);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <NavBar />
    </Provider>
  </React.StrictMode>,
  document.getElementById('navBar')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
